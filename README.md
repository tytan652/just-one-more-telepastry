# Just One More TelePastry

I want a second Custom Cake, this mod is for you.

## About
A TelePastries add-on which add another custom cake, it's a copy carbon of the original TelePastries custom cake which
allows you to add another custom dimension cake.

## Why just a carbon copy ?
I really new to Minecraft modding and directly creating a kubeJS add-on for Telepatries is not the first step,
but may be the next.

## License
* Just One More TelePastry is licensed under the MIT License
    - (c) 2021 tytan652
package xyz.tytanium.jom_telepastry.config;

import net.minecraftforge.common.ForgeConfigSpec;
import net.minecraftforge.common.ForgeConfigSpec.BooleanValue;
import net.minecraftforge.common.ForgeConfigSpec.ConfigValue;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.config.ModConfig;
import org.apache.commons.lang3.tuple.Pair;
import xyz.tytanium.jom_telepastry.JomTelePastry;

import java.util.Arrays;
import java.util.List;

public class PastryConfig {

    public static class Server {
        public final BooleanValue consumeCustomCake;
        public final ConfigValue<? extends String> customCakeName;
        public final ConfigValue<? extends String> customCakeDimension;
        public final ConfigValue<List<? extends String>> customCakeRefillItem;

        Server(ForgeConfigSpec.Builder builder) {
            builder.comment("General settings")
                    .push("General");

            consumeCustomCake = builder
                    .comment("Defines if the Custom Cake gets partly consumed when eaten [default: true]")
                    .define("consumeCustomCake", true);

            customCakeName = builder
                    .comment("Defines the name of the cake [default: \"Another Custom\"]")
                    .define("customCakeName", "Another Custom", o -> (o instanceof String));

            customCakeDimension = builder
                    .comment("Defines the dimension bound to the custom cake [default: \"minecraft:the_overworld\"]")
                    .define("customCakeDimension", "minecraft:the_overworld", o -> (o instanceof String));

            String[] customItems = new String[]
                    {
                            "minecraft:bedrock"
                    };

            customCakeRefillItem = builder
                    .comment("Set the refill items used by the Custom Cake (Only change if you know what you're doing) [modid:itemname]")
                    .defineList("customCakeRefillItem", () -> Arrays.asList(customItems), o -> (o instanceof String));

            builder.pop();
        }
    }

    public static final ForgeConfigSpec serverSpec;
    public static final Server SERVER;

    static {
        final Pair<Server, ForgeConfigSpec> specPair = new ForgeConfigSpec.Builder().configure(Server::new);
        serverSpec = specPair.getRight();
        SERVER = specPair.getLeft();
    }

    @SubscribeEvent
    public static void onLoad(final ModConfig.Loading configEvent) {
        JomTelePastry.LOGGER.debug("Loaded TelePastries' config file {}", configEvent.getConfig().getFileName());
    }

    @SubscribeEvent
    public static void onFileChange(final ModConfig.Reloading coBlocknfigEvent) {
        JomTelePastry.LOGGER.debug("TelePastries' config just got changed on the file system!");
    }
}

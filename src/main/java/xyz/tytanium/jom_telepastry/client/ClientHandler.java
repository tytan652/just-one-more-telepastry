package xyz.tytanium.jom_telepastry.client;

import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.RenderTypeLookup;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import xyz.tytanium.jom_telepastry.init.PastryRegistry;

public class ClientHandler {
    public static void doClientStuff(final FMLClientSetupEvent event) {
        RenderTypeLookup.setRenderLayer(PastryRegistry.CUSTOM_CAKE.get(), RenderType.cutout());
    }
}

package xyz.tytanium.jom_telepastry.generator;

import com.mrbysco.telepastries.blocks.cake.BlockCakeBase;
import net.minecraft.block.Block;
import net.minecraft.data.DataGenerator;
import net.minecraft.data.LootTableProvider;
import net.minecraft.data.loot.BlockLootTables;
import net.minecraft.loot.LootTable;
import net.minecraft.loot.LootTableManager;
import net.minecraft.loot.ValidationTracker;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.model.generators.BlockStateProvider;
import net.minecraftforge.client.model.generators.ConfiguredModel;
import net.minecraftforge.client.model.generators.ModelFile;
import net.minecraftforge.common.data.ExistingFileHelper;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.GatherDataEvent;
import xyz.tytanium.jom_telepastry.Reference;
import xyz.tytanium.jom_telepastry.init.PastryRegistry;

import java.util.Map;

import static xyz.tytanium.jom_telepastry.init.PastryRegistry.CUSTOM_CAKE;

@Mod.EventBusSubscriber(bus = Mod.EventBusSubscriber.Bus.MOD)
public class PastryGenerator {

    @SubscribeEvent
    public static void gatherData(GatherDataEvent event) {
        DataGenerator generator = event.getGenerator();
        ExistingFileHelper helper = event.getExistingFileHelper();

        if (event.includeServer()) {
            generator.addProvider(new Loots(generator));
        }
        if (event.includeClient()) {
            generator.addProvider(new BlockStates(generator, helper));
        }
    }

    private static class Loots extends LootTableProvider {
        public Loots(DataGenerator gen) {
            super(gen);
        }

        @Override
        protected void validate(Map<ResourceLocation, LootTable> map, ValidationTracker validationresults) {
            map.forEach((name, table) -> LootTableManager.validate(validationresults, name, table));
        }

        private class PastryBlocks extends BlockLootTables {
            @Override
            protected void addTables() {
                this.add(CUSTOM_CAKE.get(), noDrop());
            }

            @Override
            protected Iterable<Block> getKnownBlocks() {
                return PastryRegistry.BLOCKS.getEntries().stream().map(RegistryObject::get)::iterator;
            }
        }
    }

    private static class BlockStates extends BlockStateProvider {

        public BlockStates(DataGenerator gen, ExistingFileHelper helper) {
            super(gen, Reference.MOD_ID, helper);
        }

        @Override
        protected void registerStatesAndModels() {
            makeCake(CUSTOM_CAKE.get(), "custom");
        }

        private void makeCake(Block block, String dimension) {
            ModelFile model = models().getBuilder(block.getRegistryName().getPath())
                    .parent(models().getExistingFile(mcLoc("block/cake")))
                    .texture("particle", "block/" + dimension + "/cake_side")
                    .texture("bottom", "block/" + dimension + "/cake_bottom")
                    .texture("top", "block/" + dimension + "/cake_top")
                    .texture("side", "block/" + dimension + "/cake_side");

            ModelFile modelSlice1 = models().getBuilder(block.getRegistryName().getPath() + "_slice1")
                    .parent(models().getExistingFile(mcLoc("block/cake_slice1")))
                    .texture("particle", "block/" + dimension + "/cake_side")
                    .texture("bottom", "block/" + dimension + "/cake_bottom")
                    .texture("top", "block/" + dimension + "/cake_top")
                    .texture("side", "block/" + dimension + "/cake_side")
                    .texture("inside", "block/" + dimension + "/cake_inner");
            ModelFile modelSlice2 = models().getBuilder(block.getRegistryName().getPath() + "_slice2")
                    .parent(models().getExistingFile(mcLoc("block/cake_slice2")))
                    .texture("particle", "block/" + dimension + "/cake_side")
                    .texture("bottom", "block/" + dimension + "/cake_bottom")
                    .texture("top", "block/" + dimension + "/cake_top")
                    .texture("side", "block/" + dimension + "/cake_side")
                    .texture("inside", "block/" + dimension + "/cake_inner");
            ModelFile modelSlice36 = models().getBuilder(block.getRegistryName().getPath() + "_slice3")
                    .parent(models().getExistingFile(mcLoc("block/cake_slice3")))
                    .texture("particle", "block/" + dimension + "/cake_side")
                    .texture("bottom", "block/" + dimension + "/cake_bottom")
                    .texture("top", "block/" + dimension + "/cake_top")
                    .texture("side", "block/" + dimension + "/cake_side")
                    .texture("inside", "block/" + dimension + "/cake_inner");
            ModelFile modelSlice4 = models().getBuilder(block.getRegistryName().getPath() + "_slice4")
                    .parent(models().getExistingFile(mcLoc("block/cake_slice4")))
                    .texture("particle", "block/" + dimension + "/cake_side")
                    .texture("bottom", "block/" + dimension + "/cake_bottom")
                    .texture("top", "block/" + dimension + "/cake_top")
                    .texture("side", "block/" + dimension + "/cake_side")
                    .texture("inside", "block/" + dimension + "/cake_inner");
            ModelFile modelSlice5 = models().getBuilder(block.getRegistryName().getPath() + "_slice5")
                    .parent(models().getExistingFile(mcLoc("block/cake_slice5")))
                    .texture("particle", "block/" + dimension + "/cake_side")
                    .texture("bottom", "block/" + dimension + "/cake_bottom")
                    .texture("top", "block/" + dimension + "/cake_top")
                    .texture("side", "block/" + dimension + "/cake_side")
                    .texture("inside", "block/" + dimension + "/cake_inner");
            ModelFile modelSlice6 = models().getBuilder(block.getRegistryName().getPath() + "_slice6")
                    .parent(models().getExistingFile(mcLoc("block/cake_slice6")))
                    .texture("particle", "block/" + dimension + "/cake_side")
                    .texture("bottom", "block/" + dimension + "/cake_bottom")
                    .texture("top", "block/" + dimension + "/cake_top")
                    .texture("side", "block/" + dimension + "/cake_side")
                    .texture("inside", "block/" + dimension + "/cake_inner");

            getVariantBuilder(block)
                    .forAllStates(state -> {
                        int bites = state.getValue(BlockCakeBase.BITES);
                        boolean untouched = bites == 0;
                        return ConfiguredModel.builder()
                                .modelFile(untouched ? model : models().getBuilder(block.getRegistryName().getPath() + "_slice" + bites)).build();
                    });
        }
    }
}

package xyz.tytanium.jom_telepastry;

import com.mrbysco.telepastries.client.ClientHandler;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.fml.DistExecutor;
import net.minecraftforge.fml.ModLoadingContext;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.config.ModConfig.Type;
import net.minecraftforge.fml.event.lifecycle.InterModEnqueueEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import xyz.tytanium.jom_telepastry.config.PastryConfig;
import xyz.tytanium.jom_telepastry.init.PastryRegistry;

@Mod(Reference.MOD_ID)
public class JomTelePastry {
    public static final Logger LOGGER = LogManager.getLogger();

    public JomTelePastry() {
        IEventBus eventBus = FMLJavaModLoadingContext.get().getModEventBus();
        ModLoadingContext.get().registerConfig(Type.COMMON, PastryConfig.serverSpec);
        FMLJavaModLoadingContext.get().getModEventBus().register(PastryConfig.class);

        eventBus.addListener(this::sendImc);

        PastryRegistry.BLOCKS.register(eventBus);
        PastryRegistry.ITEMS.register(eventBus);

        DistExecutor.unsafeRunWhenOn(Dist.CLIENT, () -> () -> {
            FMLJavaModLoadingContext.get().getModEventBus().addListener(ClientHandler::doClientStuff);
        });
    }

    public void sendImc(InterModEnqueueEvent event) {

    }
}

package xyz.tytanium.jom_telepastry.item;

import net.minecraft.block.Block;
import net.minecraft.item.BlockItem;
import net.minecraft.item.ItemStack;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import xyz.tytanium.jom_telepastry.config.PastryConfig;

public class CustomCakeBlockItem extends BlockItem {
    public CustomCakeBlockItem (Block blockIn, Properties builder) {
        super(blockIn, builder);
    }

    @Override
    public ITextComponent getName(ItemStack stack) {
        return new TranslationTextComponent(this.getDescriptionId(stack), PastryConfig.SERVER.customCakeName.get());
    }
}
